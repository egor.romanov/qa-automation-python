# **Variables with input (entering) data**
input_value1 = int(input())
input_value2 = int(input())
input_value3 = int(input())
# Variables with sum and multiple of 3 entered values:
multiple_value = (input_value1 * input_value2 * input_value3)
sum_value = (input_value1 + input_value2 + input_value3)

# Boolean-Check: This part of code is comparing values to find max value of sum and multiple:
if (multiple_value > sum_value):
    print(multiple_value)
elif (multiple_value < sum_value):
    print(sum_value)
else:
    print(multiple_value, ' #Values of sum and multiple are equal')
