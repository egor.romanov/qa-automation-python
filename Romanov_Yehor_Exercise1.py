
# Entering data into system and reverse this data in another variable
string_data = input()
reversed_data = string_data[::-1]

# Boolean-check: Is data actually palindrome or not?
if reversed_data == string_data:
    print('Yes, it\'s palindrome')
else:
    print('No, it isn\'t palindrome')