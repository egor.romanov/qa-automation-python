# **Variables with input (entering) data**
a = int(input())
b = int(input())

# 1) 3rd variable methode:
container = b
b = a
a = container
print(a, b)

""" Returning variables to the beginning of code: """
a, b = b, a

# 2) Python style methode:
a, b = b, a
print(a, b)

""" Returning variables to the beginning of code: """
a, b = b, a

# 3) Arithmetic calculation methode:
a = a - b
b = b + a
a = b - a
print(a, b)
