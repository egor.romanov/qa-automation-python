# **Variables**:
Input_Data = int(input())
# This variable that is the beginning of Fibonacci list:
Fibonacci_List = [0, 1]
# This variable is counting last element by the Fibonacci formule:
Fibonacci_Counter = list()

# **Boolean-Check: Is entered data bigger than 0 or not (True variant)?**
if (Input_Data > 0):
    # While cycle that will create and print in console the result of Fibonacci code:
    for i in range(Input_Data - 1):
        Fibonacci_Counter = Fibonacci_List[-1] + Fibonacci_List[-2]
        Fibonacci_List.append(Fibonacci_Counter)
    Fibonacci_List.pop(0)
    print(Fibonacci_List)
# **Boolean-Check: Is entered data bigger than 0 or not? (False variant)**
else:
    print('Entered data must be bigger than 0')
