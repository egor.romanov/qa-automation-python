# **Variables with input (entering) data**
input_string1 = input()
input_string2 = input()

# Boolean-Check: Is 1st string (String_1 variable) including 2nd string (String_2 variable)?
if (input_string2 in input_string1):
    print('Yes, 1st string includes 2nd string')
else:
    print('No, 1st sting doesn\'t include 2nd string')
