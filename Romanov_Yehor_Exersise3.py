# **Variables with integer inputed data**:
a = int(input())
b = int(input())
c = int(input())


# Boolean-Check: Is variable "a" containing zero or not?
if (a != 0):
    # Variable (D) that contains Discriminant
    D = (b ** 2) - (4 * (a * c))
    # Boolean-Check: Checking decision-ways by the Discriminant variable (D)
    if (D > 0):
        x1 = ((-b) + pow(D, 0.5)) / (2 * a)
        x2 = ((-b) - pow(D, 0.5)) / (2 * a)
        print('Your result is:', 'x1 =' , x1, 'x2 =', x2, sep=' ')
    elif (D == 0):
        x = (-b) / (2 * a)
        print('Your result is:', x, sep=' ')
    elif (D < 0):
        print('Your result is: roots of the equation are not found' )
    # Boolean-Check: Is variable "c" contains zero or not?
    elif (c == 0):
        x1 = 0
        x2 = -(b / a)
        print('Your result is:', 'x1 =' , x1, 'x2 =', x2, sep=' ')
    # Boolean-Check: Is variable "b" contains zero or not?
    # ((it includes checking for different between "a" and "b" variables))
    elif (b == 0):
        if ((a > 0) and (c < 0)) or ((a > 0) and (c < 0)):
            x1 = pow(c / a)
            x2 = -(pow(c / a))
            print('Your result is:', 'x1 =' , x1, 'x2 =', x2, sep=' ')
        else:
            print('Your equation has not any result')
    # Boolean-Check: Are "b" and "c" variables equal to 0 (together)
    elif ((b == 0) and (c == 0)):
        print('Your result is: x =', 0, sep=' ')
# Boolean-Check: Is variable "a" contains zero or not?
else:
    print('Variable "a" can not be 0, please try another way')
